<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1494207487046" ID="ID_479092950" MODIFIED="1494802522835" TEXT="Microservice Principles">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Reference Twelve Factors? 12factor.net
      </li>
      <li>
        Small <i>Autonomous</i>&#160;services that <i>Work Together</i>
      </li>
    </ul>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1494207554315" FOLDED="true" ID="ID_1782258412" MODIFIED="1494802525869" POSITION="right" TEXT="Modeled around Business Domain">
<node CREATED="1494282292665" FOLDED="true" ID="ID_468822121" MODIFIED="1494802510033" TEXT="Bounded Contexts">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      aka seams
    </p>
  </body>
</html></richcontent>
<node CREATED="1494382051472" ID="ID_563415142" MODIFIED="1494382117574">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font face="MinionPro" size="11.000000pt"><span style="font-family: MinionPro; font-size: 11.000000pt">&#8220;a specific responsibility enforced by explicit boundaries.&#8221; </span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <div title="Page 51" class="page">
      <div class="layoutArea">
        <div class="column">
          <p>
            <font size="8.000000pt" face="MinionPro" color="rgb(60.000000%, 0.000000%, 0.000000%)"><span style="font-style: italic; color: rgb(60.000000%, 0.000000%, 0.000000%); font-size: 8.000000pt; font-family: MinionPro"><i>http://bit.ly/bounded-context-explained </i></span></font>
          </p>
        </div>
      </div>
    </div>
  </body>
</html></richcontent>
</node>
<node CREATED="1494382232274" FOLDED="true" ID="ID_538172466" MODIFIED="1494802504538" TEXT="Shared &amp; Hidden Models">
<node CREATED="1494382300457" ID="ID_315341778" MODIFIED="1494382319937" TEXT="Services don&apos;t expose unnecessary details"/>
</node>
</node>
<node CREATED="1494283939121" ID="ID_1551749199" MODIFIED="1494283945757" TEXT="Domain-Driven Design"/>
<node CREATED="1494382408719" ID="ID_1030849903" MODIFIED="1494382421724" TEXT="Focus on Business Capabilities"/>
</node>
<node CREATED="1494207596822" FOLDED="true" ID="ID_119827597" MODIFIED="1494802525872" POSITION="left" TEXT="Culture of Automation">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      or...
    </p>
    <ul>
      <li>
        Infrastructure Automation
      </li>
      <li>
        Automated Testing
      </li>
      <li>
        Continuous Delivery
      </li>
    </ul>
  </body>
</html></richcontent>
<node CREATED="1494282307591" ID="ID_1100704531" MODIFIED="1494282314491" TEXT="Automated Testing"/>
<node CREATED="1494282315398" ID="ID_1204840626" MODIFIED="1494282339172" TEXT="Deploy the Same Way Everywhere"/>
<node CREATED="1494282339479" ID="ID_165418004" MODIFIED="1494282344982" TEXT="Continuous Delivery"/>
<node CREATED="1494282348085" ID="ID_1213837785" MODIFIED="1494282353557" TEXT="Environment Definitions"/>
<node CREATED="1494282353902" ID="ID_1313510738" MODIFIED="1494282360570" TEXT="Custom Images"/>
<node CREATED="1494282360841" ID="ID_1153341951" MODIFIED="1494282372688" TEXT="Immutable Servers"/>
</node>
<node CREATED="1494207613443" FOLDED="true" ID="ID_749210745" MODIFIED="1494802525879" POSITION="right" TEXT="Hide (Internal) Implementation Details">
<node CREATED="1494282380878" ID="ID_798708962" MODIFIED="1494282395647" TEXT="Bounded Contexts (again?)"/>
<node CREATED="1494282397964" ID="ID_319105188" MODIFIED="1494282405392" TEXT="Hide Their Databases"/>
<node CREATED="1494282406605" ID="ID_1226884674" MODIFIED="1494282453190" TEXT="(Event) Data Pumps"/>
<node CREATED="1494282453428" ID="ID_440633058" MODIFIED="1494282477279" TEXT="Technology-Agnostic APIs"/>
</node>
<node CREATED="1494207655847" FOLDED="true" ID="ID_1776954380" MODIFIED="1494802525880" POSITION="left" TEXT="Decentralize All the Things">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Keyword: <b>AUTONOMY </b>
    </p>
    <p>
      ...
    </p>
    <ul>
      <li>
        Self-Service
      </li>
      <li>
        Shared Governance
      </li>
      <li>
        Internal Open Source
      </li>
      <li>
        Dumb Pipes, Smart Endpoints
      </li>
      <li>
        Choregraphy Over Orchestration
      </li>
    </ul>
  </body>
</html></richcontent>
<node CREATED="1494282674193" ID="ID_1235262456" MODIFIED="1494282680363" TEXT="Self-Service"/>
<node CREATED="1494282680952" ID="ID_851537181" MODIFIED="1494282691610" TEXT="Teams own their Services"/>
<node CREATED="1494282707542" ID="ID_996058485" MODIFIED="1494282731349" TEXT="Internal Open Source"/>
<node CREATED="1494282731683" ID="ID_1179620442" MODIFIED="1494282746708" TEXT="Align Teams to the Organization"/>
<node CREATED="1494282747539" ID="ID_547072588" MODIFIED="1494282779113" TEXT="Shared Governance"/>
<node CREATED="1494282759372" FOLDED="true" ID="ID_513836462" MODIFIED="1494802510041" TEXT="Prefer Choreography over Orchestration">
<node CREATED="1494382777814" ID="ID_750019456" MODIFIED="1494382850733" TEXT="Choreography: Event-driven">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Microservice observes event, acts accordingly in response.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1494382851622" ID="ID_288931956" MODIFIED="1494382879772" TEXT="Orchestration: &quot;god&quot; services"/>
<node CREATED="1494382920349" ID="ID_332117895" MODIFIED="1494382948012" TEXT="Choreo leads to loosely-coupled services"/>
</node>
<node CREATED="1494282781055" ID="ID_1362625487" MODIFIED="1494282799849" TEXT="Dumb Middleware with Smart Endpoints"/>
</node>
<node CREATED="1494207742465" FOLDED="true" ID="ID_1185205353" MODIFIED="1494802525881" POSITION="right" TEXT="Deploy Independently">
<node CREATED="1494282862165" FOLDED="true" ID="ID_331101243" MODIFIED="1494802510042" TEXT="Coexist Versioned Endpoints">
<node CREATED="1494383943361" ID="ID_1800578375" MODIFIED="1494383982795" TEXT="Same microservice, not two seperate versions of similar code"/>
</node>
<node CREATED="1494282881029" FOLDED="true" ID="ID_191739324" MODIFIED="1494802510043" TEXT="Avoid Tightly Bound Client/Server Stub Generation">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Microservice doesn't <i>need</i>&#160;to be RESTful, but it definitely promotes best practices.
    </p>
  </body>
</html></richcontent>
<node CREATED="1494383280171" ID="ID_1153275683" MODIFIED="1494383294190" TEXT="Example: Java RMI"/>
</node>
<node CREATED="1494282903131" ID="ID_690849238" MODIFIED="1494282912042" TEXT="One-Service-Per-Host"/>
<node CREATED="1494282913595" FOLDED="true" ID="ID_1495517879" MODIFIED="1494802510045" TEXT="Release Techniques">
<node CREATED="1494283132390" ID="ID_36477435" MODIFIED="1494283138421" TEXT="Blue/Green Deployments"/>
<node CREATED="1494283138982" ID="ID_1449080559" MODIFIED="1494283142593" TEXT="Canary"/>
</node>
<node CREATED="1494282930034" ID="ID_140011692" MODIFIED="1494282941028" TEXT="Consumer-Driven Contracts"/>
<node CREATED="1494282941647" ID="ID_541986" MODIFIED="1494282959820" TEXT="Consumers Should Decide When They Update Themselves"/>
</node>
<node CREATED="1494207752743" FOLDED="true" ID="ID_926966778" MODIFIED="1494802525882" POSITION="left" TEXT="Isolate Failure">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Avoid the Distributed Single Point of Failure
      </li>
      <li>
        Strangler App?
      </li>
    </ul>
  </body>
</html></richcontent>
<node CREATED="1494283037475" FOLDED="true" ID="ID_283412140" MODIFIED="1494802510046" TEXT="Don&apos;t Treat Remote Calls like Local Calls">
<node CREATED="1494383204637" FOLDED="true" ID="ID_949794565" MODIFIED="1494802507755" TEXT="Overhead">
<node CREATED="1494383224341" ID="ID_165509543" MODIFIED="1494383229294" TEXT="Serialization"/>
<node CREATED="1494383230348" ID="ID_271787945" MODIFIED="1494383233288" TEXT="Network"/>
</node>
<node CREATED="1494383092014" ID="ID_1216410764" MODIFIED="1494383137680" TEXT="First fallacy of distributed computing: &quot;The network is reliable&quot;"/>
</node>
<node CREATED="1494283048517" ID="ID_1897713153" MODIFIED="1494283053797" TEXT="Antifragility"/>
<node CREATED="1494283054565" ID="ID_393430053" MODIFIED="1494283059201" TEXT="Timeouts"/>
<node CREATED="1494283059475" FOLDED="true" ID="ID_1178507332" MODIFIED="1494802510047" TEXT="Mitigation Patterns">
<node CREATED="1494283164529" ID="ID_363635386" MODIFIED="1494283169976" TEXT="Bulkheads"/>
<node CREATED="1494283170534" ID="ID_810821515" MODIFIED="1494283174925" TEXT="Circuit Breakers"/>
</node>
<node CREATED="1494283072747" ID="ID_1870458943" MODIFIED="1494283105058" TEXT="CAP Theorem"/>
</node>
<node CREATED="1494207760813" FOLDED="true" ID="ID_626680958" MODIFIED="1494802525883" POSITION="right" TEXT="Highly Observable">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Stats Pages
      </li>
      <li>
        Aggregation (generalized)
      </li>
      <li>
        Correlation IDs
      </li>
    </ul>
  </body>
</html></richcontent>
<node CREATED="1494283205808" FOLDED="true" ID="ID_1015517198" MODIFIED="1494802510048" TEXT="Semantic Monitoring">
<node CREATED="1494384220548" ID="ID_523616711" MODIFIED="1494384227781" TEXT="Synthetic Transactions"/>
</node>
<node CREATED="1494283223020" FOLDED="true" ID="ID_1729569183" MODIFIED="1494802510049" TEXT="Aggregation">
<node CREATED="1494383529738" ID="ID_382412327" MODIFIED="1494383531963" TEXT="Logs"/>
<node CREATED="1494383532644" ID="ID_152327599" MODIFIED="1494383534886" TEXT="Stats"/>
</node>
<node CREATED="1494283247222" ID="ID_1347023864" MODIFIED="1494283255284" TEXT="Correlation IDs"/>
</node>
<node CREATED="1494281932368" FOLDED="true" ID="ID_505598497" MODIFIED="1494802525884" POSITION="left" TEXT="Consumer First">
<node CREATED="1494283683933" FOLDED="true" ID="ID_1151018293" MODIFIED="1494802510050" TEXT="Documentation">
<node CREATED="1494283688804" ID="ID_79129617" MODIFIED="1494283690887" TEXT="Swagger"/>
<node CREATED="1494284347014" ID="ID_280935825" MODIFIED="1494284349103" TEXT="HAL"/>
</node>
<node CREATED="1494283692536" ID="ID_1161718026" MODIFIED="1494283699993" TEXT="API Gateways"/>
<node CREATED="1494283700380" ID="ID_1502101652" MODIFIED="1494283706572" TEXT="Service Discovery"/>
<node CREATED="1494283707266" ID="ID_1611886496" MODIFIED="1494283721301" TEXT="Humane Registries"/>
</node>
</node>
</map>
