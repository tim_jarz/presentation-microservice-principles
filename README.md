Presentation based on _Building Microservices_ by Sam Newman.

Principles:

1. Modeled around Business Domain
2. Culture of Automation
3. Hide Implementation Details
4. Decentralize All the Things
5. Deploy Independently
6. Consumer First
7. Isolate Failure
8. Highly Observable

Also checkout:

* https://www.slideshare.net/spnewman/principles-of-microservices-velocity
* http://shop.oreilly.com/product/0636920043935.do
