# Principles

1. Modeled around business domain
2. Culture of automation
3. Hide implementation details
4. Decentralize all the things
5. Deploy independently
6. Consumer first
7. Isolate failure
8. Highly observable

note:
    Put your speaker notes here.
    You can see them pressing 's'.
