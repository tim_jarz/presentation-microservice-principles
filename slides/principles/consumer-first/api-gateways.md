## Consumer first

### API gateways

Server-side aggregation endpoint
<!-- .element: class="fragment" -->

_Backend for Frontend (BFF)_
<!-- .element: class="fragment" -->

note:

- Aggregate multiple services into one external endpoint
- Danger: creating a god service
- Have multiple BFFs - one per external product
