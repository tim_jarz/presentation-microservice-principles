## Consumer first

### Documentation

Swagger
<!-- .element: class="fragment" -->

HAL
<!-- .element: class="fragment" -->

note:

- Hypertext Application Language
- HAL: step beyond Swagger, uses _hypermedia_ (aka links) to aid discoverability
