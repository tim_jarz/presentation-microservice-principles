## Consumer first

### Humane registries

People (programmers)
<!-- .element: class="fragment" -->

Wiki
<!-- .element: class="fragment" -->

Automation as backup
<!-- .element: class="fragment" -->

note:

- Automated discovery doesn't do anything for a developer sitting at an IDE...
- Wiki reduces friction, easy for reading and contributions
- Data gets stale, create tools to fetch data from source control, CI, issue tracking...
