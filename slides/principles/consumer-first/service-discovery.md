## Consumer first

### Service discovery

How do we find these services?
<!-- .element: class="fragment" -->

It's complicated...
<!-- .element: class="fragment" -->

DNS
<!-- .element: class="fragment" -->

Dynamic Service Registries 
<!-- .element: class="fragment" -->

(Apache Zookeeper, Hashicorp Consul)
<!-- .element: class="fragment" -->

note:
