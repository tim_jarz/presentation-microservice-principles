## Culture of automation

### Automated testing

Increased complexity
<!-- .element: class="fragment" -->

Reduce time-to-release
<!-- .element: class="fragment" -->

Different types of tests
<!-- .element: class="fragment" -->

- Unit tests
<!-- .element: class="fragment" -->
- Acceptance tests
<!-- .element: class="fragment" -->
- Property tests
<!-- .element: class="fragment" -->

note:

- Acceptance testing: fit-finess, end-to-end, large scope
- Property testing: reponse time, scalability, performance security