## Culture of automation

### Continuous delivery

Automated testing + Infrastructure automation
<!-- .element: class="fragment" -->

Create pipelines
<!-- .element: class="fragment" -->

One build per microservice
<!-- .element: class="fragment" -->

note:

- Automated testing *together* with infrastructure automation enables good continuous delivery practices.
