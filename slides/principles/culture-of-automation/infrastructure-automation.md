## Culture of automation

### Infrastructure automation

Configuration management
<!-- .element: class="fragment" -->

Spin up / down hosts
<!-- .element: class="fragment" -->

note:

- VMs, containers, etc.
- Custom images, immutable / phoenix servers
