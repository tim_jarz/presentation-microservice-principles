## Decentralize all the things

### Align teams to the organization

Conway's Law
<!-- .element: class="fragment" -->

> <!-- .element: class="fragment" -->
> Any organization that designs a system [...] will inevitably produce a design whose structure is a copy of the organization’s communication structure.

note:

- April 1968 -- almost 50 years
- Summarized by Eric S. Raymond in _The New Hacker's Dictionary_ as "If you have four groups working on a compiler, you'll get a 4-pass compiler."
- Reduce friction!
