## Decentralize all the things

### Prefer choreography over orchestration

Choreography: event-driven
<!-- .element: class="fragment" -->

Orchestration: "god" services
<!-- .element: class="fragment" -->

Loosely-coupled vs tightly-coupled
<!-- .element: class="fragment" -->

note:

- Microservices observe events, react
- God services can violate bounded contexts
- Typically rely on knowing internal behavior of subservient services