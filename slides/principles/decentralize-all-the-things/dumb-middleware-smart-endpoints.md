## Decentralize all the things

### Dumb middleware with smart endpoints

Remember enterprise service buses?
<!-- .element: class="fragment" -->

Let's not do that again
<!-- .element: class="fragment" -->

Simple message broker
<!-- .element: class="fragment" -->

note:

- ESB's typically had business logic in the bus instead of services.
- Simple brokers = less magic