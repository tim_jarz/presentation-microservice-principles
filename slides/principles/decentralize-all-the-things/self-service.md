## Decentralize all the things

### Self-service

Avoid bottlenecks
<!-- .element: class="fragment" -->

Remove development friction
<!-- .element: class="fragment" -->

note:

- Devs would avoid creating new services if high start-up costs, long wait, etc.
- Leads into next slide: teams own their services