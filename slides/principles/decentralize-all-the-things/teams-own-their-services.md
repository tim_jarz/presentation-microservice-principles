## Decentralize all the things

### Teams own their services

Team is totally responsible for service
<!-- .element: class="fragment" -->

Includes deployment
<!-- .element: class="fragment" -->

note:

- Again: autonomy is important in microservice architecture
- Promotes better code, since you support what you wrote
