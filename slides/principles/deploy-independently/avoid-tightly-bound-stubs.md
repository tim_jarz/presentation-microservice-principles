## Deploy independently

### Avoid tightly-bound client/server stub generation

RPC-style integration
<!-- .element: class="fragment" -->

Binary stubs require regeneration for changes
<!-- .element: class="fragment" -->

Lock-step client/server releases
<!-- .element: class="fragment" -->

note:

- Breaks autonomy, ability to make changes quickly
