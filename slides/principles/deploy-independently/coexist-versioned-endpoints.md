## Deploy independently

### Coexist versioned endpoints

Gradual consumer migration to vNext API
<!-- .element: class="fragment" -->

Avoid impact of breaking changes
<!-- .element: class="fragment" -->

note:

- Internally, can transform a v1 request into v2, and the response from v2 to v1.
- Good idea to have metrics to know when endpoint can be retired.
