## Deploy independently

### Consumer-driven contracts

Define consumer expectations in tests
<!-- .element: class="fragment" -->

See breaking changes
<!-- .element: class="fragment" -->

Determine resolution
<!-- .element: class="fragment" -->

note:

- Producer and consumer teams collaborate on tests.
- Fix the problem, or start working with team impacted by breaking change