## Deploy independently

### One service per "host"

Impact to other services on shared host
<!-- .element: class="fragment" -->

note:

- Definition of a host has changed significantly over past decade