## Deploy independently

### Release techniques

Blue/green
<!-- .element: class="fragment" -->

Canary
<!-- .element: class="fragment" -->

note:

- Blue/green deployment: two versions of software deployed, but only one receives real requests.
- Deploy new version, run smoke tests, redirect traffic.
- Keep "blue" around for a little bit as fallback.
- Can accomplish zero-downtime deployments.
- Canary releasing: side-by-side, compare current and new services.
- Divert _some_ traffic, or copy production load and compare.
