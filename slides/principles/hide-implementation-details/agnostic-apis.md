## Hide implementation details

### Technology-agnostic APIs

Remember SOA?
<!-- .element: class="fragment" -->

> <!-- .element: class="fragment" -->
> Those who cannot remember the past are condemned to repeat it.
> -- _George Santayana_


10 years ago, would you have predicted Node.JS?
<!-- .element: class="fragment" -->

Keep your options open
<!-- .element: class="fragment" -->

note:
- SOA implementations often required expensive middleware
- Will we always be a .NET or Java shop?
- Web-facing service that is useful internally
- Devs like to experiment
