## Hide implementation details

### Bounded contexts

_again..._
<!-- .element: class="fragment" -->

note:
    Parking garage... controller doesn't need to know design of the gate, just sends signals.
