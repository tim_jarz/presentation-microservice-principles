## Hide implementation details

### Data pumps

One solution for reporting
<!-- .element: class="fragment" -->

Change our schema at will
<!-- .element: class="fragment" -->

Reporting DB/system is completely external
<!-- .element: class="fragment" -->

note:
    Typically a separate exe pumps data from svc db to reporting db
    Pump exe owned by svc's team