## Isolate failure

### Correlation IDs

GUID attached to incoming request
<!-- .element: class="fragment" -->

Tracked throughout request/response lifecycle
<!-- .element: class="fragment" -->

Transaction rollback
<!-- .element: class="fragment" -->
 
note:
    Put your speaker notes here.
    You can see them pressing 's'.
