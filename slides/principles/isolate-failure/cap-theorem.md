## Isolate failure

### CAP theorem

Consistency, availability, partition tolerance
<!-- .element: class="fragment" -->

Pick two
<!-- .element: class="fragment" -->

AP: simpler to build & scale
<!-- .element: class="fragment" -->

CP: challenge of distributed consistency
<!-- .element: class="fragment" -->

CA: not an option (network is a partition)
<!-- .element: class="fragment" -->

note:
    Put your speaker notes here.
    You can see them pressing 's'.
