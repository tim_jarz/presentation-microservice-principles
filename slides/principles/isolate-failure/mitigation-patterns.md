## Isolate failure

### Mitigation patterns

Circuit breaker
<!-- .element: class="fragment" -->

Bulkheads
<!-- .element: class="fragment" -->

note:
    Require a definition of _failed request_ (timeouts, 500's...)
    "Blown" circuit breaker can attempt health checks, reset itself
    Bulkheads seal off compartments in a ship
    Shared connection pool example -- isolation needed
