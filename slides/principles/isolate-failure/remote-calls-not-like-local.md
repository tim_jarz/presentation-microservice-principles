## Isolate failure

### Don't treat remote calls like local calls

Network performance
<!-- .element: class="fragment" -->

Network reliability
<!-- .element: class="fragment" -->

Data marshalling performance
<!-- .element: class="fragment" -->

note:
    Don't make it so opaque callers don't realize there's a network call involved in RPC
