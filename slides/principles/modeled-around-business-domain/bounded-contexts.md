## Modeled around the business domain

### Bounded contexts

> <!-- .element: class="fragment" data-fragment-index="1" -->
> a specific responsibility enforced by explicit boundaries
<!-- .element: class="fragment" data-fragment-index="1" -->

note:
    Bounded contexts are sometimes called _seams_.
    Each has own models, language / terminology.
    Example: B2C software -- who is the _customer_ in the current context?
