## Modeled around the business domain

### Domain-driven design

Divide domain into bounded contexts
<!-- .element: class="fragment" -->

Define explicit interrelationships
<!-- .element: class="fragment" -->

![Cover of _Domain-Driven Design_ by Eric Evans](/resources/domain-driven-design.jpg)<!-- .element: height="25%" width="25%" class="fragment" -->

note:
    https://martinfowler.com/bliki/BoundedContext.html
