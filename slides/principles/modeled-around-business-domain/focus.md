## Modeled around the business domain

### Focus on business capabilities

Not the technical underpinnings
<!-- .element: class="fragment" -->

note:
    Tough for me... who doesn't like to geek out on good architecture?
    Don't let the architecture influence the models.
