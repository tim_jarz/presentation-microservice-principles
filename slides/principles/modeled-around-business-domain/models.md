## Modeled around the business domain

### Shared and hidden models

Don't expose unnecessary details to consumers
<!-- .element: class="fragment" -->

note:
    Evaluate your models, what do consumers *need* to know?
    Failure to do so can cause issues with _autonomous_ aspect.
    Can create situations where consumers are inadvertantly too tightly coupled.
