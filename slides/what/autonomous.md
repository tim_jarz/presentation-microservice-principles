## Microservices are **autonomous**

### Decoupled
<!-- .element: class="fragment" -->

Separate entities
<!-- .element: class="fragment" -->

Communicate via network
<!-- .element: class="fragment" -->

Change independently
<!-- .element: class="fragment" -->

Well-defined consumer API
<!-- .element: class="fragment" -->

note:

- Separate entities: isolated processes
- Network calls enforce separation
- Services can be changed and deployed without impacting consumers
- Consider tech-agnostic APIs to avoid constraining consumers
- Golden Rule: can you make a change to a service and deploy it by itself without changing anything else?
