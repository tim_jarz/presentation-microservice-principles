## Microservices are **small**

### Single Responsibility Pattern
<!-- .element: class="fragment" -->

> <!-- .element: class="fragment" -->
> Gather together those things that change for the same reason, and separate those things that change for different reasons. 
(Robert C. Martin, aka Uncle Bob)

### So... how small?
<!-- .element: class="fragment" -->

"Small enough and no smaller"
<!-- .element: class="fragment" -->

note:

    - "two week" or "single sprint" write/rewrite potential

    - consider diminishing returns, like how long it takes to spin up new project, etc. compared to how often you'll need to change/deploy
